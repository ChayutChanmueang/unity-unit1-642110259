using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chanmeaung.GameDev3.UnityBasic
{
    public class SimpleHealthPointComponent : MonoBehaviour
    {
        [SerializeField] public const float MAX_HP = 100;

        [SerializeField]
        public float m_Healthpoint
        {
            get
            {
                return m_Healthpoint;
            }
            set
            {
                if (value > 0)
                {
                    if (value <= MAX_HP)
                    {
                        m_Healthpoint = value;
                    }
                    else
                    {
                        m_Healthpoint = MAX_HP;
                    }
                }
            }
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
