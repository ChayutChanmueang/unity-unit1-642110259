using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chanmeaung.GameDev3.UnityBasic
{
    public enum ItemType
    {
        COIN, 
        BIGCOIN, 
        POWERUP, 
        POWERDOWN,
    }
}
